# CMake build system for Firebird3 project #

## Description ##

### Features ###

Pros:

* Full featured 2-phase build (like `run_all.bat` or `./autoconf.sh && make`) including `examples`, `icu` and other libraries
* Сonfig autogeneration on all platforms (including Windows)
* Crossplatform support
* Can be built with mulpitple cores on any plaftorms, e.g. `make -j8` or `/MP` switch in `Visual Studio`
* Projects can be generated for numerous IDEs, e.g. `Visual Studio`, `Xcode`, `CodeBlocks` and others. See [CMake Generators](http://www.cmake.org/cmake/help/v3.0/manual/cmake-generators.7.html)
* Supports proper build in multiconfiguration IDEs like `Visual Studio` and `Xcode`. They can have different build configurations like `Debug`, `MinSizeRel`, `Release`, `RelWithDebInfo`
* Easier to maintain
* It is possible to integrate **building** (not just unpacking) of external dependencies into the build process (zlib, ICU, editline sources)
* Sources in all IDEs with such support are grouped like in current `Visual Studio` projects (tested in `Visual Studio`, `Xcode`)

Neutral:

* In-source builds are not allowed and at the moment not possible (see F.A.Q.). All generated files will be placed in a separate build directory.

Cons:

* Developers should learn some basics of CMake.
* Possible bugs after initial commit related to untested features, plarforms
* (In case of full replacement of current build system): Time to migrate and move _all_ useful features to new build system, refreshing server scripts etc.

### Why? ###

* To simplify and unify the build process
* To replace existing two separate build systems with one CMake build system
* To help beginner developers to start code investigations rather than build investigations
* For simple integration of new features (libs, plugins, binaries, other build steps)

### To be done ###
* install/uninstall related work (configure install files; make install/uninstall commands working)
* check preprocessor defs and options (like DEV_BUILD, DEBUG), compiler and linker options, RPATH related issues
* (from autoconf): volatile keyword, binreloc detection

### Build completes on ###

* Windows 8.1 64-bit, Visual Studio 12 32-bit
* Windows 8.1 64-bit, Visual Studio 12 64-bit
* Ubuntu 14.04.1 64-bit, makefiles
* OS X 10.10.1, AppleClang 6.0, makefiles
* OS X 10.10.1, AppleClang 6.0, Xcode project
* CentOS 7, gcc 4.8.2
* FreeBSD 10.1, clang 3.4.1

### Tried to build, but failed ###

* mingw 4.6.1
* mingw-w64 4.8.2, 4.9.1 (as they do not have some definitions from ws2def.h - file `src/remote/inet.cpp`)
* CentOS 6 (CMake), gcc 4.4.7
* CentOS 6 (current Firebird build system), it requires autconf >= 2.67, but 2.63 found

### Testing done  ###

Output binaries were tested a little on Win32 and results are the same with Firebird trunk build system.

### Notes ###
* `editline` library from source tree is not used. Build always uses `libreadline` from the system on *nix and does not use it on win32 at the moment.
* Unimplemented configure feature: volatile keyword. Not needed?
* Some not OS-related features differ in win32/linux builds. Do it in a same way?
* Some things require developers' attention.
* `make && make copy_files && make -B` triggers assertion on linux because of `firebird/intl/fbintl.conf`. If you remove this file, build completes successfully.

### F.A.Q. ###

Q: Will you be able to maintain and improve this build system if integrated to Firebird trunk?
> A: Yes.

Q: Why do **YOU** need this?
> A: I need this to better understanding of sources, build process, files and libraries organization; to see all build steps in one place, one solution in IDE. With such build system it's easier for me to investigate different things in Firebird code in case if I would like to contribute to the project.

Q: Why CMake?
> A: CMake provides a high-level language of build configuration unlike autotools. Also it's quite popular (used in some well known projects like KDE, OpenCV, LLVM and others).

Q: What about backporting to earlier versions of Firebird (2.5, 2.1, ...)?
> A: Maybe. Only to 2.5.x (latest) if needed. Other people can backport for older versions themselves.

Q: Why two step build? Firebird3 can be built in single step/without special boot step/etc...
> A: `.cpp` files after `.epp` building still slightly differ from each other between `GPRE boot` and `GPRE master` build steps. I don't know how these changes can affect the rest of the binaries, so I split the build into the complete steps like in Firebird 2.x. Here's the [diff](http://pastebin.com/deqqtVdy). This question is open for Firebird developers.

> Also this can simplify backporting to 2.5.x where two step build is necessary.

Q: What OSes does CMake support?
> A: CMake supports `Windows`, `GNU/Linux`, `OS X`. The ports are available for `*BSD` systems. Also CMake works for `IBM AIX`, `HP-UX`, `Solaris` and **probably** can be built for some other platforms.

Q: What IDEs does CMake support?
> A: `Visual Studio`, `Xcode`, `CodeBlocks`, `Eclipse`, `QtCreator` and others. See [Supported IDEs](http://www.cmake.org/cmake/help/v3.0/manual/cmake-generators.7.html)

Q: How should I do changes to build system? Using generated projects?
> A: No. You should open from your favourite IDE appropriate `CMakeLists.txt` file which contains build steps for a library or an executable you want to change. Then you should make changes (add or remove file(s), change dependencies, output directories etc.) in this file, save it and run the build. CMake will automatically refresh build and project files with your changes, configure them and run building of the Firebird.

Q: Why are in-source builds disabled?
> A: CMake produces a lot of build files that will appear in sources. Firebird itself produces many generated files during the build too. It makes very hard to do commits, navigate through the sources and maintain them. Now Makefile produced by CMake replaces Makefile from trunk, so it can lead to errors on commit stage. Also there is some other objective reasons related to CMake you can find in the internet.




## OK, then guide me into it! ##

### Requirements ###

* `sed`, `svn`, `git` in path as in current build system

### How to ###

**Here we assume that you are a developer and atleast know a little about build instruments, VCS (svn, git) and have them installed and configured on your system.**

**Steps will be described as in *nix OS.**

Get the fresh copy of the Firebird trunk repository via svn or manually:

    svn co svn://svn.code.sf.net/p/firebird/code/firebird/trunk firebird_cmake

Get the fresh copy of the Firebird CMake build system from this repository using git or just downloading the archive file. And copy all files from it to Firebird sources directory with all replaces.

    git clone https://bitbucket.org/egor_pugin/firebird_cmake.git firebird_cmake_bs
    cp -R firebird_cmake_bs/* firebird_cmake/

[Download latest CMake](http://www.cmake.org/download/) and install it.

Run appropriate CMake command to create build files in the build directory. Project files and solutions for IDEs can be found in the build folder.

    mkdir build
    cd build
    cmake ..

Or tricky way (`-H` sets home directory to `.`, `-B` sets build directory to `build`):

    cmake -H. -Bbuild

Out-of-source build can be done in any directory on filesystem, not only inside Firebird sources directory like above (firebird_cmake/build).

    cmake -H"path to firebird sources with cmake" -B"any directory on filesystem except as in -H"

Then go to your build directory and start the build. N - number of parallel build jobs.

    make
    make -jN





### Examples ###

#### Make build files ####

Create build files on any platform (for `Visual Studio` it generates projects and solution):

    cmake -H. -Bbuild

Create projects and solution for special `Visual Studio` version installed on the system (e.g. 10):

    cmake -H. -Bbuild -G "Visual Studio 10"

Create projects and solution for `Visual Studio` 64-bit:

    cmake -H. -Bbuild64 -G "Visual Studio 12 Win64"

Create projects and solution for Xcode:

    cmake -H. -Bbuild_xcode -G Xcode

Create build files for MinGW and compile. Make sure MinGW binaries directory is in your path.

    cmake -H. -Bwin_mingw -G "MinGW Makefiles"
    mingw32-make -C win_mingw

#### Build Firebird ####

To run the build on Windows with `Visual Studio` you can open the solution in `build` directory, select `Debug` or `Release` or other configuration. Or other way from console (if appropriate vcvars*.bat was executed):

    cd build
    devenv firebird.sln /Build

To run build on *nix (feel free to set other number of jobs - number of cores in your system):

    make
    make -j3
    make -j4
    make -j8

To copy doc/include/examples and other misc files manually call building `copy_files` target. Or select it in your IDE and press build button.

    make copy_files

#### CentOS 6, 7 specific ####

    yum install svn git cmake gcc gcc-c++ libicu-devel readline-devel

#### Run IDE project ####

To run Visual Studio solution open win\firebird.sln.

To run Xcode project open build_xcode/firebird.xcodeproj

To run QtCreator and build firebird, open main (in source root directory) CMakeLists.txt file from QtCreator.

#### Build log ####

Example build log after executing following commands on linux:

    cd firebird
    mkdir build
    cd build
    cmake ..
    make
    make copy_files

[Log](http://pastebin.com/TkupCxhk)

### Related links ###

* [CMake](http://www.cmake.org/)
* [Download CMake](http://www.cmake.org/download/)