#
# firebird (trunk)
#
#  This file has following organization:
#  1. preprocess
#  2. custom build steps (file generators)
#  3. libraries
#  4. shared libraries
#  5. executables
#  6. subdirectories
#  7. copy other files to output dir (docs, includes, ...)
#

include(SourceGroups)

################################################################################
#
# PREPROCESS
#
################################################################################

########################################
# PREPROCESS epp boot and master files
########################################

set(epp_boot_internal_files
    burp/backup.epp
    burp/restore.epp
    burp/OdsDetection.epp
    utilities/gstat/dba.epp
)
set(epp_boot_ocxx_files
    isql/extract.epp
    isql/isql.epp
    isql/show.epp
)
set(epp_boot_files
    alice/alice_meta.epp
    gpre/std/gpre_meta.epp
    utilities/stats.epp
    yvalve/array.epp
    yvalve/blob.epp
)
set(epp_boot_gds_files
    dsql/metd.epp
    dsql/DdlNodes.epp
    dsql/PackageNodes.epp
    jrd/dfw.epp
    jrd/dpm.epp
    jrd/dyn_util.epp
    jrd/fun.epp
    jrd/grant.epp
    jrd/ini.epp
    jrd/met.epp
    jrd/pcmet.epp
    jrd/scl.epp
    jrd/Function.epp
)
set(epp_master_files
    auth/SecurityDatabase/LegacyManagement.epp
    msgs/build_file.epp
    misc/codes.epp
    qli/help.epp
    qli/meta.epp
    qli/proc.epp
    qli/show.epp
)

epp_process(boot    epp_boot_internal_files gpre_boot -lang_internal -n -m)
epp_process(boot    epp_boot_ocxx_files     gpre_boot -lang_internal -n -ids -ocxx)
epp_process(boot    epp_boot_files          gpre_boot -n -m)
epp_process(boot    epp_boot_gds_files      gpre_boot -n -ids -gds_cxx)

epp_process(master  epp_boot_internal_files boot_gpre -n -m)
epp_process(master  epp_boot_ocxx_files     boot_gpre -n -ids -ocxx)
epp_process(master  epp_boot_files          boot_gpre -n -m)
epp_process(master  epp_boot_gds_files      boot_gpre -n -ids -gds_cxx)
epp_process(master  epp_master_files        boot_gpre -n -m)


################################################################################
#
# CUSTOM BUILD STEPS
#
################################################################################

########################################
# BUILD STEP databases
########################################

set(databases_src
    ${CMAKE_CURRENT_SOURCE_DIR}/dbs/security.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/facilities2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/history2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/locales.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/messages2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/sqlstates.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/symbols2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/system_errors2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/transmsgs.de_DE2.sql
    ${CMAKE_CURRENT_SOURCE_DIR}/msgs/transmsgs.fr_FR2.sql
)
add_custom_command(
    OUTPUT security.fdb
    DEPENDS
        boot_isql
        ${CMAKE_CURRENT_SOURCE_DIR}/dbs/security.sql
    VERBATIM
    #
    COMMAND ${CMAKE_COMMAND} -E remove security3.fdb
    COMMAND ${CMAKE_COMMAND} -E echo "create database 'security3.fdb';" > create_db.sql
    COMMAND boot_isql -q -i create_db.sql
    COMMAND boot_isql -q security3.fdb -i ${CMAKE_CURRENT_SOURCE_DIR}/dbs/security.sql
    COMMAND ${CMAKE_COMMAND} -E copy_if_different security3.fdb security.fdb
)
add_custom_command(
    OUTPUT metadata.fdb
    DEPENDS
        boot_gbak
        ${CMAKE_SOURCE_DIR}/builds/misc/metadata.gbak
    #
    COMMAND ${CMAKE_COMMAND} -E remove metadata.fdb
    COMMAND boot_gbak -r ${CMAKE_SOURCE_DIR}/builds/misc/metadata.gbak metadata.fdb
)
set(isql_exec_msg boot_isql -q msg.fdb -i ${CMAKE_CURRENT_SOURCE_DIR}/msgs)
add_custom_command(
    OUTPUT msg.fdb
    VERBATIM
    DEPENDS
        boot_isql
        metadata.fdb
        security.fdb
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/facilities2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/history2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/locales.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/messages2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/sqlstates.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/symbols2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/system_errors2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/transmsgs.de_DE2.sql
        ${CMAKE_CURRENT_SOURCE_DIR}/msgs/transmsgs.fr_FR2.sql
    #
    COMMAND ${CMAKE_COMMAND} -E remove msg.fdb
    COMMAND ${CMAKE_COMMAND} -E echo "create database 'msg.fdb';" > create_db.sql
    COMMAND boot_isql -q -i create_db.sql
    COMMAND ${isql_exec_msg}/msg.sql
    #
    COMMAND echo loading facilities
    COMMAND ${isql_exec_msg}/facilities2.sql
    COMMAND echo loading sql states
    COMMAND ${isql_exec_msg}/sqlstates.sql
    COMMAND echo loading locales
    COMMAND ${isql_exec_msg}/locales.sql
    COMMAND echo loading history
    COMMAND ${isql_exec_msg}/history2.sql
    COMMAND echo loading messages
    COMMAND ${isql_exec_msg}/messages2.sql
    COMMAND echo loading symbols
    COMMAND ${isql_exec_msg}/symbols2.sql
    COMMAND echo loading system errors
    COMMAND ${isql_exec_msg}/system_errors2.sql
    COMMAND echo loading French translation
    COMMAND ${isql_exec_msg}/transmsgs.fr_FR2.sql
    COMMAND echo loading German translation
    COMMAND ${isql_exec_msg}/transmsgs.de_DE2.sql
)
add_custom_command(
    OUTPUT help.fdb
    DEPENDS
        boot_gbak
        metadata.fdb
        ${CMAKE_SOURCE_DIR}/builds/misc/help.gbak
    #
    COMMAND ${CMAKE_COMMAND} -E remove help.fdb
    COMMAND boot_gbak -r ${CMAKE_SOURCE_DIR}/builds/misc/help.gbak help.fdb
)
add_custom_target(databases
    DEPENDS
        boot_engine12
        msg.fdb
        help.fdb
    SOURCES
        ${databases_src}
)
project_group(databases "Custom build steps")


########################################
# BUILD STEP messages
########################################

add_custom_command(
    OUTPUT firebird.msg
    DEPENDS build_msg codes
    #
    COMMAND ${CMAKE_COMMAND} -E remove firebird.msg
    COMMAND build_msg -D msg.fdb -P ./ -F firebird.msg -L all
    COMMAND build_msg -D msg.fdb -P ./ -F firebird.msg
)
add_custom_target(messages DEPENDS firebird.msg)
project_group(messages "Custom build steps")
add_custom_command(
    TARGET messages
    POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different firebird.msg ${output_dir}/firebird.msg
)


########################################
# BUILD_STEP parse
########################################

set(parse_src
    ${CMAKE_CURRENT_SOURCE_DIR}/dsql/parse.y
    ${CMAKE_CURRENT_SOURCE_DIR}/dsql/btyacc_fb.ske
)
add_custom_command(
    OUTPUT y_tab.h y_tab.c
    DEPENDS
        btyacc
        ${parse_src}
    COMMAND sed -n "/%type .*/p" ${CMAKE_CURRENT_SOURCE_DIR}/dsql/parse.y > types.y
    COMMAND sed "s/%type .*//" ${CMAKE_CURRENT_SOURCE_DIR}/dsql/parse.y > y.y
    COMMAND btyacc -l -d -S ${CMAKE_CURRENT_SOURCE_DIR}/dsql/btyacc_fb.ske y.y
    COMMAND ${CMAKE_COMMAND} -E copy_if_different y_tab.h include/gen/parse.h
    COMMAND ${CMAKE_COMMAND} -E copy_if_different y_tab.c dsql/parse.cpp
    COMMENT "Generating parse.cpp, parse.h"
    VERBATIM
)
add_custom_target(parse
    DEPENDS y_tab.h y_tab.c
    SOURCES ${parse_src}
)
project_group(parse "Custom build steps")
set_source_files_properties(dsql/parse.cpp include/gen/parse.h PROPERTIES GENERATED TRUE)


################################################################################
#
# LIBRARIES
#
################################################################################

###############################################################################
# LIBRARY boot_alice
###############################################################################

file(GLOB alice_src "alice/*.cpp" "alice/*.h")

set(alice_generated_src
    alice/alice_meta.epp
)
add_epp_suffix(alice_generated_src boot)
add_epp_suffix(alice_generated_src master)

add_library                 (boot_alice ${alice_src} ${alice_generated_src_boot})
project_group               (boot_alice Boot)


###############################################################################
# LIBRARY alice
###############################################################################

add_library                 (alice ${alice_src} ${alice_generated_src_master})


###############################################################################
# LIBRARY boot_burp
###############################################################################

file(GLOB burp_src "burp/*.cpp" "burp/*.h")

set(burp_generated_src
    burp/backup.epp
    burp/OdsDetection.epp
    burp/restore.epp
)
add_epp_suffix(burp_generated_src boot)
add_epp_suffix(burp_generated_src master)

add_library                 (boot_burp ${burp_src} ${burp_generated_src_boot})
project_group               (boot_burp Boot)


###############################################################################
# LIBRARY burp
###############################################################################

add_library                 (burp ${burp_src} ${burp_generated_src_master})


###############################################################################
# LIBRARY common
###############################################################################

file(GLOB common_src "common/*.cpp" "common/classes/*.cpp" "common/config/*.cpp" "common/os/${OS_DIR}/*.cpp")
file(GLOB_RECURSE common_include "common/*.h")

if (APPLE)
    file(GLOB common_os_src "common/os/posix/*.cpp")
    list(REMOVE_ITEM common_os_src ${CMAKE_CURRENT_BINARY_DIR}/common/os/posix/mod_loader.cpp)
endif()

add_library(common ${common_src} ${common_os_src} ${common_include})


################################################################################
#
# SHARED LIBRARIES
#
################################################################################

########################################
# SHARED LIBRARY boot_yvalve
########################################

file(GLOB yvalve_src "yvalve/*.cpp" "yvalve/config/os/${OS_DIR}/*.c*")
file(GLOB_RECURSE yvalve_include "yvalve/*.h")

set(yvalve_src ${yvalve_src}
    auth/SecureRemotePassword/client/SrpClient.cpp
    auth/SecurityDatabase/LegacyClient.cpp
    plugins/crypt/arc4/Arc4.cpp
    remote/client/BlrFromMessage.cpp
    remote/client/interface.cpp
)
add_src_win32(yvalve_src
    jrd/os/win32/ibinitdll.cpp
)
set(yvalve_generated_src
    yvalve/array.epp
    yvalve/blob.epp
)
add_epp_suffix(yvalve_generated_src boot)
add_epp_suffix(yvalve_generated_src master)

add_library                 (yvalve_common OBJECT ${yvalve_src} ${yvalve_include})
add_dependencies            (yvalve_common parse)

add_library                 (boot_yvalve SHARED $<TARGET_OBJECTS:yvalve_common> ${yvalve_generated_src_boot} ${VERSION_RC})
target_link_libraries       (boot_yvalve remote common libtommath ${LIB_Ws2_32} ${LIB_mpr} ${LIB_readline} ${LIB_dl} ${LIB_iconv} ${LIB_CoreFoundation})
set_exported_symbols        (boot_yvalve firebird)
set_output_directory_unix   (boot_yvalve lib)
project_group               (boot_yvalve Boot)


########################################
# SHARED LIBRARY yvalve
########################################

add_library                 (yvalve SHARED $<TARGET_OBJECTS:yvalve_common> ${yvalve_generated_src_master} ${yvalve_include} ${VERSION_RC})
target_link_libraries       (yvalve remote common libtommath ${LIB_Ws2_32} ${LIB_mpr} ${LIB_readline} ${LIB_dl} ${LIB_iconv} ${LIB_CoreFoundation})
set_exported_symbols        (yvalve firebird)
set_output_directory_unix   (yvalve lib)
set_target_properties       (yvalve PROPERTIES OUTPUT_NAME fbclient)


########################################
# SHARED LIBRARY boot_engine12
########################################

file(GLOB engine12_src
    "dsql/*.cpp"
    "jrd/*.cpp"
    "jrd/extds/*.cpp"
    "jrd/recsrc/*.cpp"
    "jrd/trace/*.cpp"
    "jrd/os/${OS_DIR}/*.cpp"
)
set(engine12_src ${engine12_src}
    lock/lock.cpp
    utilities/gsec/gsec.cpp
    utilities/gstat/ppg.cpp
    utilities/nbackup/nbackup.cpp    
    # parse
    dsql/parse.cpp
    include/gen/parse.h    
)
add_src_apple(engine12_src
    jrd/os/posix/unix.cpp
)
set(engine12_generated_src
    dsql/DdlNodes.epp
    dsql/metd.epp
    dsql/PackageNodes.epp
    jrd/dfw.epp
    jrd/dpm.epp
    jrd/dyn_util.epp
    jrd/fun.epp
    jrd/Function.epp
    jrd/grant.epp
    jrd/ini.epp
    jrd/met.epp
    jrd/pcmet.epp
    jrd/scl.epp
    utilities/gstat/dba.epp
)
add_epp_suffix(engine12_generated_src boot)
add_epp_suffix(engine12_generated_src master)

file(GLOB_RECURSE engine12_include "dsql/*.h" "jrd/*.h" include/gen/iberror.h)

add_library                 (engine12_common OBJECT ${engine12_src} ${engine12_include})
add_dependencies            (engine12_common parse)

add_library                 (boot_engine12 SHARED $<TARGET_OBJECTS:engine12_common> ${engine12_generated_src_boot} ${parse_src} ${VERSION_RC})
target_link_libraries       (boot_engine12 boot_alice boot_burp common boot_yvalve)
set_output_directory        (boot_engine12 plugins)
set_exported_symbols        (boot_engine12 fbplugin)
copy_and_rename_lib         (boot_engine12 Engine12)
project_group               (boot_engine12 Boot)


########################################
# SHARED LIBRARY engine12
########################################

add_library                 (engine12 SHARED $<TARGET_OBJECTS:engine12_common> ${engine12_generated_src_master} ${parse_src} ${VERSION_RC})
target_link_libraries       (engine12 alice burp common yvalve)
add_dependencies            (engine12 messages) # possible build during build_msg or codes run
set_target_properties       (engine12 PROPERTIES OUTPUT_NAME Engine12)
set_output_directory        (engine12 plugins)
set_exported_symbols        (engine12 fbplugin)


###############################################################################
# SHARED LIBRARY intl
###############################################################################

file(GLOB intl_src "intl/*.cpp" "intl/*.h")

add_library                 (intl SHARED ${intl_src} ${VERSION_RC})
target_link_libraries       (intl common boot_yvalve)
set_target_properties       (intl PROPERTIES OUTPUT_NAME fbintl)
set_output_directory        (intl intl)


########################################
# SHARED LIBRARY ib_util
########################################

add_library                 (ib_util SHARED extlib/ib_util.cpp extlib/ib_util.h ${VERSION_RC})
set_exported_symbols        (ib_util ib_util)
set_output_directory_unix   (ib_util lib)


########################################
# SHARED LIBRARY ib_udf
########################################

add_library                 (ib_udf SHARED extlib/ib_udf.cpp extlib/ib_udf.h ${VERSION_RC})
target_link_libraries       (ib_udf ib_util)
set_target_properties       (ib_udf PROPERTIES PREFIX "")
set_exported_symbols        (ib_udf ib_udf)
set_output_directory        (ib_udf UDF)


########################################
# SHARED LIBRARY legacy_usermanager
########################################

set(legacy_usermanager_generated_src
    auth/SecurityDatabase/LegacyManagement.epp
)
add_epp_suffix(legacy_usermanager_generated_src master)

add_library                 (legacy_usermanager SHARED ${legacy_usermanager_generated_src_master} auth/SecurityDatabase/LegacyManagement.h ${VERSION_RC})
target_link_libraries       (legacy_usermanager common yvalve)
set_target_properties       (legacy_usermanager PROPERTIES OUTPUT_NAME Legacy_UserManager)
set_output_directory        (legacy_usermanager plugins)
set_exported_symbols        (legacy_usermanager fbplugin)


########################################
# SHARED LIBRARY udr_engine
########################################

set(udr_engine_src
    plugins/udr_engine/UdrEngine.cpp
)
add_library                 (udr_engine SHARED ${udr_engine_src} ${VERSION_RC})
target_link_libraries       (udr_engine common yvalve)
set_output_directory        (udr_engine plugins)
set_exported_symbols        (udr_engine udr_engine)


########################################
# SHARED LIBRARY fbudf
########################################

set(fbudf_src
    extlib/fbudf/fbudf.cpp
    extlib/fbudf/stdafx.cpp

    extlib/fbudf/fbudf.txt
    extlib/fbudf/fbudf.sql

    jrd/ibase.h
)
file(GLOB fbudf_include "extlib/fbudf/*.h")

add_library                 (fbudf SHARED ${fbudf_src} ${fbudf_include} ${VERSION_RC})
target_link_libraries       (fbudf common yvalve)
set_target_properties       (fbudf PROPERTIES PREFIX "")
set_output_directory        (fbudf UDF)


########################################
# SHARED LIBRARY srp
########################################

add_library                 (srp SHARED auth/SecureRemotePassword/manage/SrpManagement.cpp ${VERSION_RC})
target_link_libraries       (srp common yvalve)
set_target_properties       (srp PROPERTIES OUTPUT_NAME Srp)
set_output_directory        (srp plugins)
set_exported_symbols        (srp fbplugin)


########################################
# SHARED LIBRARY legacy_auth
########################################

add_library                 (legacy_auth SHARED auth/SecurityDatabase/LegacyServer.cpp ${VERSION_RC})
target_link_libraries       (legacy_auth common yvalve)
set_target_properties       (legacy_auth PROPERTIES OUTPUT_NAME Legacy_Auth)
set_output_directory        (legacy_auth plugins)
set_exported_symbols        (legacy_auth fbplugin)


################################################################################
#
# EXECUTABLES
#
################################################################################

########################################
# EXECUTABLE gpre_boot
########################################

set(gpre_boot_src
    gpre/boot/gpre_meta_boot.cpp
    yvalve/gds.cpp
)

add_executable              (gpre_boot ${gpre_boot_src} ${VERSION_RC})
target_link_libraries       (gpre_boot gpre_common common ${LIB_Ws2_32})


########################################
# EXECUTABLE boot_gpre
########################################

set(gpre_generated_src
    gpre/std/gpre_meta.epp
)
add_epp_suffix(gpre_generated_src boot)
add_epp_suffix(gpre_generated_src master)

add_executable              (boot_gpre ${gpre_generated_src_boot} ${VERSION_RC})
target_link_libraries       (boot_gpre gpre_common common boot_yvalve)
project_group               (boot_gpre Boot)


########################################
# EXECUTABLE gpre
########################################

add_executable              (gpre ${gpre_generated_src_master} ${VERSION_RC})
target_link_libraries       (gpre gpre_common common yvalve)


########################################
# EXECUTABLE build_msg
########################################

set(build_msg_generated_src
    msgs/build_file.epp
)
add_epp_suffix(build_msg_generated_src master)

add_executable              (build_msg ${build_msg_generated_src_master} ${VERSION_RC})
target_link_libraries       (build_msg common yvalve)


########################################
# EXECUTABLE codes
########################################

set(codes_generated_src
    misc/codes.epp
)
add_epp_suffix(codes_generated_src master)

add_executable              (codes ${codes_generated_src_master} ${VERSION_RC})
target_link_libraries       (codes yvalve)
add_custom_command(
    TARGET codes
    POST_BUILD
    COMMAND codes ${CMAKE_CURRENT_SOURCE_DIR}/include/gen ${CMAKE_SOURCE_DIR}/lang_helpers
)


########################################
# EXECUTABLE gstat
########################################

set(gstat_src
    jrd/btn.cpp
    jrd/ods.cpp
    utilities/gstat/main/gstatMain.cpp
    utilities/gstat/ppg.cpp
)
set(gstat_generated_src
    utilities/gstat/dba.epp
)
add_epp_suffix(gstat_generated_src master)

add_executable              (gstat ${gstat_src} ${gstat_generated_src_master} ${VERSION_RC})
target_link_libraries       (gstat common yvalve)


########################################
# EXECUTABLE fb_lock_print
########################################

add_executable              (fb_lock_print lock/print.cpp ${VERSION_RC})
target_link_libraries       (fb_lock_print common yvalve)


########################################
# EXECUTABLE fbguard
########################################

add_src_win32(fbguard_src
    iscguard/cntl_guard.cpp
    iscguard/iscguard.cpp
    remote/server/os/win32/chop.cpp

    iscguard/iscguard.rc
)
add_src_unix(fbguard_src
    utilities/guard/guard.cpp
    utilities/guard/util.cpp
)

add_executable              (fbguard WIN32 ${fbguard_src})
target_link_libraries       (fbguard common yvalve ${LIB_comctl32} ${LIB_version})


########################################
# EXECUTABLE fbtracemgr
########################################

set(fbtracemgr_src
    jrd/trace/TraceCmdLine.cpp
    utilities/fbtracemgr/traceMgrMain.cpp
)

add_executable              (fbtracemgr ${fbtracemgr_src} ${VERSION_RC})
target_link_libraries       (fbtracemgr common yvalve)


###############################################################################
# EXECUTABLE gfix
###############################################################################

add_executable              (gfix alice/main/aliceMain.cpp ${VERSION_RC})
target_link_libraries       (gfix alice common yvalve)


###############################################################################
# EXECUTABLE boot_gbak
###############################################################################

file(GLOB gbak_include "burp/*.h")

add_executable              (boot_gbak burp/main/burpMain.cpp ${gbak_include} ${VERSION_RC})
target_link_libraries       (boot_gbak boot_burp common boot_yvalve)
project_group               (boot_gbak Boot)


###############################################################################
# EXECUTABLE gbak
###############################################################################

add_executable              (gbak burp/main/burpMain.cpp ${gbak_include} ${VERSION_RC})
target_link_libraries       (gbak burp yvalve common)


###############################################################################
# EXECUTABLE gsplit
###############################################################################

add_executable              (gsplit burp/split/spit.cpp burp/split/spit.h ${VERSION_RC})
target_link_libraries       (gsplit burp common yvalve)


###############################################################################
# EXECUTABLE boot_isql
###############################################################################

file(GLOB isql_src "isql/*.cpp" "isql/*.h")

set(isql_generated_src
    isql/extract.epp
    isql/isql.epp
    isql/show.epp
)
add_epp_suffix(isql_generated_src boot)
add_epp_suffix(isql_generated_src master)

add_executable              (boot_isql ${isql_src} ${isql_generated_src_boot} ${VERSION_RC})
target_link_libraries       (boot_isql common boot_yvalve)
project_group               (boot_isql Boot)

if (WIN32)
    add_custom_command(
        TARGET boot_isql
        POST_BUILD
        # remove
        COMMAND ${CMAKE_COMMAND} -E remove ${output_dir}/icudt52l.dat ${output_dir}/icudt52l_empty.dat
        #
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/icu/icudt52l.dat ${output_dir}/icudt52l.dat
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/icu/icudt52l_empty.dat ${output_dir}/icudt52l_empty.dat
    )

    string(FIND ${CMAKE_EXE_LINKER_FLAGS} "/machine:x64" arch)
    if (NOT ${arch} EQUAL -1)
        set(arch "x64")
    else()
        set(arch "Win32")
    endif()
    add_custom_command(
        TARGET boot_isql
        POST_BUILD
        # icu
        # remove
        COMMAND ${CMAKE_COMMAND} -E remove ${output_dir}/icudt52.dll ${output_dir}/icuin52.dll ${output_dir}/icuuc52.dll
        #
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/icu/${arch}/Release/bin/icudt52.dll ${output_dir}/icudt52.dll
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/icu/${arch}/Release/bin/icuin52.dll ${output_dir}/icuin52.dll
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/icu/${arch}/Release/bin/icuuc52.dll ${output_dir}/icuuc52.dll
        # zlib
        COMMAND ${CMAKE_COMMAND} -E remove ${output_dir}/zlib1.dll
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/extern/zlib/${arch}/zlib1.dll ${output_dir}/zlib1.dll
    )
endif()


###############################################################################
# EXECUTABLE isql
###############################################################################

add_executable              (isql ${isql_src} ${isql_generated_src_master} ${VERSION_RC})
target_link_libraries       (isql common yvalve)


###############################################################################
# EXECUTABLE qli
###############################################################################

file(GLOB qli_src "qli/*.cpp" "qli/*.h")
set(qli_generated_src
    qli/help.epp
    qli/meta.epp
    qli/proc.epp
    qli/show.epp
)
add_epp_suffix(qli_generated_src master)

add_executable          (qli ${qli_src} ${qli_generated_src_master} ${VERSION_RC})
target_link_libraries   (qli common yvalve)


################################################################################
#
# subdirectories
#
################################################################################

add_subdirectory("gpre")
add_subdirectory("remote")
add_subdirectory("utilities")

###############################################################################
#
# copy files to output dir
#
###############################################################################

add_custom_target(copy_files
    #ALL # uncomment this to copy files every build
    DEPENDS
        databases
        employee_db
        makeHeader
    # databases
    COMMAND ${CMAKE_COMMAND} -E copy_if_different security3.fdb ${output_dir}/security3.fdb
    COMMAND ${CMAKE_COMMAND} -E copy_if_different help.fdb ${output_dir}/help/help.fdb
    # configs, text files
    COMMAND sed "/@UDF_COMMENT@/d" < ${CMAKE_SOURCE_DIR}/builds/install/misc/firebird.conf.in > ${output_dir}/firebird.conf    
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/misc/databases.conf.in ${output_dir}/databases.conf
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/misc/fbintl.conf ${output_dir}/intl/fbintl.conf
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/misc/plugins.conf ${output_dir}/plugins.conf
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/misc/IPLicense.txt ${output_dir}/IPLicense.txt
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/misc/IDPLicense.txt ${output_dir}/IDPLicense.txt
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/utilities/ntrace/fbtrace.conf ${output_dir}/fbtrace.conf
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/plugins/udr_engine/udr_engine.conf ${output_dir}/plugins/udr_engine.conf
    # udf
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/extlib/ib_udf.sql ${output_dir}/UDF/ib_udf.sql
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/extlib/ib_udf2.sql ${output_dir}/UDF/ib_udf2.sql
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/extlib/fbudf/fbudf.sql ${output_dir}/UDF/fbudf.sql
    # docs
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/ChangeLog ${output_dir}/doc/ChangeLog
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/doc/WhatsNew ${output_dir}/doc/WhatsNew
    # examples
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/api ${output_dir}/examples/api
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/dbcrypt ${output_dir}/examples/dbcrypt
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/examples/empbuild/employe2.sql ${output_dir}/examples/empbuild/employe2.sql
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_BINARY_DIR}/examples/employe2.fdb ${output_dir}/examples/empbuild/employee.fdb
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/include ${output_dir}/examples/include
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/interfaces ${output_dir}/examples/interfaces
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/package ${output_dir}/examples/package
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/stat ${output_dir}/examples/stat
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/udf ${output_dir}/examples/udf
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/udr ${output_dir}/examples/udr
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/stat ${output_dir}/examples/stat
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/examples/functions.c ${output_dir}/examples/functions.c
    # headers    
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/extlib/ib_util.h ${output_dir}/include/ib_util.h
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/include/gen/iberror.h ${output_dir}/include/iberror.h
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/src/yvalve/perf.h ${output_dir}/include/perf.h
)
project_group(copy_files "Custom build steps")

# headers
file(GLOB files "${CMAKE_SOURCE_DIR}/src/include/firebird/*.h")
foreach(F ${files})
    get_filename_component(name ${F} NAME)
    add_custom_command(TARGET copy_files POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${F} ${output_dir}/include/firebird/${name})
endforeach()

# docs
file(GLOB files "${CMAKE_SOURCE_DIR}/doc/README.*")
foreach(F ${files})
    get_filename_component(name ${F} NAME)
    add_custom_command(TARGET copy_files POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${F} ${output_dir}/doc/${name})
endforeach()

file(GLOB files "${CMAKE_SOURCE_DIR}/doc/sql.extensions/README.*")
foreach(F ${files})
    get_filename_component(name ${F} NAME)
    add_custom_command(TARGET copy_files POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${F} ${output_dir}/doc/sql.extensions/${name})
endforeach()

if (WIN32)
    add_custom_command(
        TARGET copy_files
        POST_BUILD
        # lib
        COMMAND ${CMAKE_COMMAND} -E copy_if_different $<CONFIG>/fbclient.lib ${output_dir}/lib/fbclient_ms.lib
        COMMAND ${CMAKE_COMMAND} -E copy_if_different $<CONFIG>/ib_util.lib ${output_dir}/lib/ib_util_ms.lib
        # installers
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/arch-specific/win32/install_classic.bat ${output_dir}/install_classic.bat
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/arch-specific/win32/install_super.bat ${output_dir}/install_super.bat
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/builds/install/arch-specific/win32/uninstall.bat ${output_dir}/uninstall.bat        
        # examples
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/build_unix ${output_dir}/examples/build_unix
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/examples/build_win32 ${output_dir}/examples/build_win32
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/examples/readme ${output_dir}/examples/readme.txt
        # headers
        COMMAND echo "#pragma message(\"Non-production version of ibase.h.\")" > ${output_dir}/include/ibase.tmp
        COMMAND echo "#pragma message(\"Using raw, unprocessed concatenation of header files.\")" >> ${output_dir}/include/ibase.tmp
    )
    set(files    
        ${CMAKE_SOURCE_DIR}/src/misc/ibase_header.txt
        ${CMAKE_SOURCE_DIR}/src/include/types_pub.h
        ${CMAKE_SOURCE_DIR}/src/common/dsc_pub.h
        ${CMAKE_SOURCE_DIR}/src/dsql/sqlda_pub.h
        ${CMAKE_SOURCE_DIR}/src/jrd/ibase.h
        ${CMAKE_SOURCE_DIR}/src/jrd/inf_pub.h
        ${CMAKE_SOURCE_DIR}/src/include/consts_pub.h
        ${CMAKE_SOURCE_DIR}/src/jrd/blr.h
        ${CMAKE_SOURCE_DIR}/src/include/gen/iberror.h
    )
    foreach(F ${files})
        string(REPLACE "/" "\\" f ${F})
        add_custom_command(TARGET copy_files POST_BUILD
            COMMAND type ${f} >> ${output_dir}/include/ibase.tmp)
    endforeach()
    add_custom_command(
        TARGET copy_files
        POST_BUILD
        COMMAND sed -f ${CMAKE_SOURCE_DIR}/src/misc/headers.sed < ${output_dir}/include/ibase.tmp > ${output_dir}/include/ibase.h
        COMMAND ${CMAKE_COMMAND} -E remove ${output_dir}/include/ibase.tmp
    )
    file(GLOB files
        "${CMAKE_SOURCE_DIR}/src/extlib/ib_udf*"
        "${CMAKE_SOURCE_DIR}/src/extlib/fbudf/*"
    )
    foreach(F ${files})
        get_filename_component(name ${F} NAME)
        add_custom_command(TARGET copy_files POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${F} ${output_dir}/examples/udf/${name})
    endforeach()
endif()

if (UNIX)
    add_executable          (makeHeader misc/makeHeader.cpp)
    set_output_directory    (makeHeader . FORCE)
    set(files
        ${CMAKE_SOURCE_DIR}/src/include/types_pub.h
        ${CMAKE_SOURCE_DIR}/src/include/consts_pub.h
        ${CMAKE_SOURCE_DIR}/src/dsql/sqlda_pub.h
        ${CMAKE_SOURCE_DIR}/src/common/dsc_pub.h
        ${CMAKE_SOURCE_DIR}/src/jrd/ibase.h
        ${CMAKE_SOURCE_DIR}/src/jrd/inf_pub.h
        ${CMAKE_SOURCE_DIR}/src/jrd/blr.h
        ${CMAKE_SOURCE_DIR}/src/include/gen/iberror.h
    )
    foreach(F ${files})
        get_filename_component(name ${F} NAME)
        add_custom_command(TARGET copy_files POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${F} ${name})
    endforeach()
    add_custom_command(TARGET copy_files POST_BUILD
        # include
        COMMAND makeHeader < ibase.h > ${output_dir}/include/ibase.h
        # examples
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/examples/readme ${output_dir}/examples/README
    )
endif()

################################################################################
